using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationMatch : MonoBehaviour
{
    [SerializeField] private Transform lightSource;
    [SerializeField] private float minAngleOffset;
    [SerializeField] private float maxAngleOffset;

    private Transform myTransform;

    // Start is called before the first frame update
    void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateShadowRotation();
    }

    private void UpdateShadowRotation()
    {
        Vector3 angle = lightSource.localEulerAngles;
        angle.y = Mathf.Clamp(angle.y, minAngleOffset, maxAngleOffset);

        myTransform.localEulerAngles = angle;
    }
}
