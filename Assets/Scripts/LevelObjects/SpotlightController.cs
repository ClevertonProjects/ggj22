using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SpotlightController : ControllableObjects
{
    [SerializeField] private float rotationSpeed;
    
    private Vector2 inputDir;    
    private Transform myTransform;

    [SerializeField] private GameObject instructionCanvas;

    // Start is called before the first frame update
    void Awake()
    {
        myTransform = transform;
    }

    private void Update()
    {        
        Move();
    }   

    private void Move()
    {
        float rotationDir = Mathf.RoundToInt(inputDir.x) == 0 ? inputDir.y : inputDir.x;
        Vector3 movingAngle = myTransform.eulerAngles - rotationSpeed * Time.deltaTime * rotationDir * Vector3.up;        
        myTransform.localEulerAngles = movingAngle;
    }

    public void ReadInputL(InputAction.CallbackContext context)
    {
        if (isActiveAndEnabled && playerInQueue != null && playerInQueue.GetComponent<PlayerStatus>().PlayerName == PlayersNames.P1)
        {
            inputDir = context.ReadValue<Vector2>();            
        }
    }

    public void ReadInputD(InputAction.CallbackContext context)
    {
        if (isActiveAndEnabled && playerInQueue != null && playerInQueue.GetComponent<PlayerStatus>().PlayerName == PlayersNames.P2)
        {
            inputDir = context.ReadValue<Vector2>();            
        }
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (isActiveAndEnabled)
            {                
                playerInQueue.GetComponent<PlayerController>().BlockInputs(false);
                playerInQueue.gameObject.SetActive(true);
                playerInQueue = null;
                inputDir = Vector2.zero;
                enabled = false;
            }
            else if (playerInQueue != null)
            {
                instructionCanvas.SetActive(false);
                playerInQueue.gameObject.SetActive(false);
                playerInQueue.GetComponent<PlayerController>().BlockInputs(true);
                
                enabled = true;
            }
        }
    }
}
