using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MovableBoxController : ControllableObjects
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float collisionRadius;
    [SerializeField] private float collisionCheckDist;

    [SerializeField] private GameObject instructionCanvas;

    private Vector2 inputDir;
    private Vector2 actualDir;
    private Transform myTransform;

    private RaycastHit[] hits = new RaycastHit[1];

    // Start is called before the first frame update
    void Awake()
    {
        myTransform = transform;
    }

    private void Update()
    {
        ClampMovement();
        Move();
    }

    private void ClampMovement()
    {
        int h = Mathf.RoundToInt(inputDir.x);
        int v = Mathf.RoundToInt(inputDir.y);
        actualDir = new Vector2(h, v);
        Vector3 dir;

        if (h != 0)
        {
            dir = Vector3.right * h;

            Debug.DrawLine(myTransform.position, myTransform.position + dir * collisionCheckDist, Color.red);

            if (Physics.SphereCastNonAlloc(myTransform.position, collisionRadius, dir, hits, collisionCheckDist, 1 << 8) > 0)
            {
                actualDir.x = 0;
            }
        }

        if (v != 0)
        {
            dir = Vector3.forward * v;

            if (Physics.SphereCastNonAlloc(myTransform.position, collisionRadius, dir, hits, collisionCheckDist, 1 << 8) > 0)
            {                
                actualDir.y = 0;
            }
        }
    }

    private void Move()
    {
        Vector3 movingDir = new Vector3(actualDir.x, 0f, actualDir.y);
        myTransform.position += moveSpeed * Time.deltaTime * movingDir;
    }

    public void ReadInputL(InputAction.CallbackContext context)
    {
        if (isActiveAndEnabled && playerInQueue != null && playerInQueue.GetComponent<PlayerStatus>().PlayerName == PlayersNames.P1)
        {
            inputDir = context.ReadValue<Vector2>();            
        }
    }

    public void ReadInputD(InputAction.CallbackContext context)
    {
        if (isActiveAndEnabled && playerInQueue != null && playerInQueue.GetComponent<PlayerStatus>().PlayerName == PlayersNames.P2)
        {
            inputDir = context.ReadValue<Vector2>();            
        }
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (isActiveAndEnabled)
            {
                playerInQueue.SetParent(null);
                playerInQueue.GetComponent<PlayerController>().BlockInputs(false);                
                inputDir = Vector2.zero;
                enabled = false;
            }
            else if (playerInQueue != null)
            {
                instructionCanvas.SetActive(false);
                playerInQueue.GetComponent<PlayerController>().BlockInputs(true);
                playerInQueue.SetParent(myTransform);
                enabled = true;
            }
        }        
    }    
}
