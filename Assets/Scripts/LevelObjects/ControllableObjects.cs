using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllableObjects : MonoBehaviour
{    
    protected Transform playerInQueue;
    
    public Transform PlayerInQueue { get { return playerInQueue; } set { playerInQueue = value; } }

    public virtual void SetObjectConfig(params object[] args)
    {

    }
}
