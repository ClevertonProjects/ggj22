using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowProjector : MonoBehaviour
{
    [SerializeField] private Transform lightSource;

    private Transform myTransform;

    // Start is called before the first frame update
    void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateShadowRotation();
    }
    
    private void UpdateShadowRotation ()
    {
        Vector3 flatSource = new Vector3(lightSource.position.x, 0f, lightSource.position.z);
        Vector3 flatPosition = new Vector3(myTransform.position.x, 0f, myTransform.position.z);

        Vector3 lightDir = flatPosition - flatSource;

        myTransform.rotation = Quaternion.LookRotation(lightDir, Vector3.up);
    }
}
