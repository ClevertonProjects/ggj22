using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCycle : MonoBehaviour
{
    private Animator anim;
    [SerializeField] private bool isOpen;
    [SerializeField] private float cycleDuration;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        if (isOpen)
        {
            anim.SetBool("IsOpen", isOpen);
        }

        StartCoroutine(DoorLoopCycle());
    }

    private IEnumerator DoorLoopCycle ()
    {
        WaitForSeconds delay = new WaitForSeconds(cycleDuration);

        while(true)
        {
            yield return delay;

            isOpen = !isOpen;
            anim.SetBool("IsOpen", isOpen);
        }
    }
}
