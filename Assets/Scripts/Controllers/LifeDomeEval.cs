using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeDomeEval : MonoBehaviour
{
    [SerializeField] private LifeDome[] lifeDomes;
    [SerializeField] private PlayerStatus playerStatus;

    // Start is called before the first frame update
    void Start()
    {
        if (lifeDomes != null && lifeDomes.Length > 0)
        {            
            StartCoroutine(OverlapEvaluationLoop());
        }
    }

    private IEnumerator OverlapEvaluationLoop ()
    {
        WaitForSeconds delay = new WaitForSeconds(0.1f);

        while (true)
        {            
            bool isSafe = false;

            foreach (LifeDome dome in lifeDomes)
            {
                if (dome.HasPlayer)
                {
                    isSafe = true;
                }
            }                     
            
            if (!isSafe)
            {
                playerStatus.GetComponent<PlayerController>().Kill();
                GetComponent<GameController>().GameOver();
                break;
            }

            yield return delay;
        }
    }

    public bool IsLifeDomeOverlap(Collider characterCollider, Collider domeCollider)
    {       
        if (characterCollider.bounds.Intersects(domeCollider.bounds))
        {
            return true;
        }
        
        return false;            
    }
}
