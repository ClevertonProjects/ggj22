using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameController : MonoBehaviour
{
    private bool isP1InPosition;
    private bool isP2InPosition;

    private bool isGameOver;
    private bool isLevelComplete;
    private int actualOptionIndex;
    [SerializeField] private ButtonsTransitions[] gameOverButtons;                           
    [SerializeField] private ButtonsTransitions levelCompleteButton;                           

    [SerializeField] private GameObject gameOverWindow;
    [SerializeField] private GameObject levelCompleteWindow;

    private void LevelComplete ()
    {
        isLevelComplete = true;
        levelCompleteWindow.SetActive(true);
        levelCompleteWindow.GetComponent<UiGroupFader>().FadeIn();
        levelCompleteButton.PlayHighlightAnimation();
    }

    public void GameOver ()
    {
        if (isGameOver || isLevelComplete) return;
        
        isGameOver = true;
        PlayerController[] players = ComponentGetter.GetMulti<PlayerController>(Tags.Player);

        for (int i = 0; i < players.Length; i++)
        {
            players[i].enabled = false;
        }

        actualOptionIndex = 0;
        gameOverWindow.SetActive(true);
        gameOverWindow.GetComponent<UiGroupFader>().FadeIn();
        gameOverButtons[actualOptionIndex].PlayHighlightAnimation();
    }

    private void CheckLevelEnd ()
    {
        if (isP1InPosition && isP2InPosition)
        {
            PlayerController[] players = ComponentGetter.GetMulti<PlayerController>(Tags.Player);

            for (int i = 0; i < players.Length; i++)
            {
                players[i].enabled = false;
            }

            LevelComplete();
        }
    }

    public void OnLevelTargetExit (PlayersNames player)
    {
        if (player == PlayersNames.P1)
        {
            isP1InPosition = false;
        }
        else
        {
            isP2InPosition = false;
        }
    }

    public void OnLevelTargetEnter (PlayersNames player)
    {
        if (player == PlayersNames.P1)
        {
            isP1InPosition = true;
        }
        else
        {
            isP2InPosition = true;
        }

        CheckLevelEnd();
    }    

    public void OnNextLevelClick ()
    {
        SceneFader fader = ComponentGetter.Get<SceneFader>(Tags.Fader);
        fader.FadeScene(FadeState.FadeIn, SceneLoading.GoToNextLevelScene);
    }

    public void OnRestartClick ()
    {
        SceneFader fader = ComponentGetter.Get<SceneFader>(Tags.Fader);
        fader.FadeScene(FadeState.FadeIn, SceneLoading.RestartLevelScene);
    }

    public void OnQuitClick ()
    {
        Application.Quit();
    }

    // <summary>
    /// Event called when any movement input is pressed.
    /// </summary>    
    public void OnMoveInput(InputAction.CallbackContext context)
    {
        Vector2 input = context.ReadValue<Vector2>();
        int roundedY = Mathf.RoundToInt(input.y);

        if (context.performed && isGameOver && roundedY != 0)
        {
            if (roundedY > 0 && actualOptionIndex > 0)
            {
                gameOverButtons[actualOptionIndex].PlayDeselectAnimation();
                actualOptionIndex--;
                gameOverButtons[actualOptionIndex].PlayHighlightAnimation();
            }
            else if (roundedY < 0 && actualOptionIndex < gameOverButtons.Length - 1)
            {
                gameOverButtons[actualOptionIndex].PlayDeselectAnimation();
                actualOptionIndex++;
                gameOverButtons[actualOptionIndex].PlayHighlightAnimation();
            }
        }
    }

    /// <summary>
    /// Event called when the action input is pressed.
    /// </summary>    
    public void OnActionInput(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (isLevelComplete)
            {                
                levelCompleteButton.PlayClickAnimation();
                OnNextLevelClick();
            }
            else if (isGameOver)
            {
                gameOverButtons[actualOptionIndex].PlayClickAnimation();                

                if (actualOptionIndex == 0)
                {
                    OnRestartClick();
                }
                else if (actualOptionIndex == 1)
                {
                    OnQuitClick();
                }                
            }
        }
    }
}
