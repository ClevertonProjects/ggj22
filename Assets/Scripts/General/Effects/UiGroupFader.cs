﻿using System.Collections;
using UnityEngine;

/*! \class UiGroupFader
 *  \brief Control the fading animation of an UI canvas group.
 */
 [RequireComponent(typeof(CanvasGroup))]
public class UiGroupFader : MonoBehaviour
{
    [SerializeField] private float duration;                                        //!< Effect duration.
    [SerializeField] private AnimationCurve fadingCurve;                            //!< Fading animation curve.

    public delegate void OnFadeEnd();

    /// Run the fading loop.
    private IEnumerator FadingLoop (float from, float to, float duration, bool isInteractable, OnFadeEnd onFadeEnd)
    {
        CanvasGroup group = GetComponent<CanvasGroup>();
        group.blocksRaycasts = isInteractable;
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float elapsedTime = 0f;
        float progression = 0f;
        float t;

        while (progression < 1f)
        {
            t = fadingCurve.Evaluate(progression);
            group.alpha = Mathf.Lerp(from, to, t);

            yield return delay;

            elapsedTime += Time.deltaTime;
            progression = elapsedTime / duration;
        }

        group.alpha = to;

        if (onFadeEnd != null)
        {
            onFadeEnd();
        }
    }

    /// <summary>
    /// Set the UI group visible.
    /// </summary>
    public void FadeIn()
    {
        FadeIn(null);
    }

    /// <summary>
    /// Set the UI group invisible.
    /// </summary>
    public void FadeOut()
    {
        FadeOut(null);
    }

    /// <summary>
    /// Fading out loop to use as a coroutine wait parameter.
    /// </summary>    
    public Coroutine YieldedFadeOut ()
    {
        return StartCoroutine(FadingLoop(1f, 0f, duration, false, null));
    }

    /// <summary>
    /// Set the UI group visible.
    /// </summary>
    public void FadeIn (OnFadeEnd fadeEndCallback)
    {
        StartCoroutine(FadingLoop(0f, 1f, duration, true, fadeEndCallback));
    }

    /// <summary>
    /// Set the UI group invisible.
    /// </summary>
    public void FadeOut (OnFadeEnd fadeEndCallback)
    {
        StartCoroutine(FadingLoop(1f, 0f, duration, false, fadeEndCallback));
    }
}
