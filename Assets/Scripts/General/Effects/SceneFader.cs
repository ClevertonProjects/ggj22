using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*! \class SceneFader
 *  \brief Controls an UI image fade effect.
 */
public class SceneFader : MonoBehaviour
{
    [SerializeField] private float fadeDuration;                        //!< Fading time duration.
    [SerializeField] private Image fadeImage;                           //!< Image used to hide the whole screen.        

    public AnimationCurve FadeCurve;                                    //!< Curve determining the fade value variation through time.  

    public delegate void OnFadeEnd();                                   //!< Callback for fade end.

    /// Routine to iterate the alpha value of fadeImage.
    private IEnumerator FadeRoutine(float toAlpha, float duration, AnimationCurve fadingCurve, OnFadeEnd onFadeEnd)
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float elapsedTime = 0f;
        float proportion = 0f;
        Color colorIterator = fadeImage.color;
        float from = colorIterator.a;

        while (proportion < 1f)
        {
            colorIterator.a = Mathf.Lerp(from, toAlpha, fadingCurve.Evaluate(proportion));
            fadeImage.color = colorIterator;

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / duration;
        }

        if (toAlpha <= 0f)
        {
            fadeImage.enabled = false;                
        }
        else
        {
            colorIterator.a = toAlpha;
            fadeImage.color = colorIterator;
        }

        if (onFadeEnd != null)
        {
            onFadeEnd();
        }
    }

    /// <summary>
    /// Starts to fade the scene accordingly to the option selected (in or out).
    /// </summary>
    public void FadeScene(FadeState fadeOption, OnFadeEnd callback = null)
    {
        fadeImage.enabled = true;
        int toAlpha = (int)fadeOption;
        StartCoroutine(FadeRoutine(toAlpha, fadeDuration, FadeCurve, callback));
    }        
}