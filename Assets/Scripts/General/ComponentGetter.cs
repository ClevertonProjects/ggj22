using UnityEngine;

/*! \class ComponentGetter
 *  \brief Generic class used to get objects' components.
 */
public class ComponentGetter
{
    /// <summary>
    /// Gets a specific component from an object.
    /// </summary>    
    public static T Get<T> (string objectTag)
    {
        return GameObject.FindGameObjectWithTag(objectTag).GetComponent<T>();
    }

    /// <summary>
    /// Gets a specific component from multiple objects.
    /// </summary>  
    public static T[] GetMulti<T> (string objectTag)
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag(objectTag);
        T[] comps = new T[objs.Length];

        for (int i = 0; i < objs.Length; i++)
        {
            comps[i] = objs[i].GetComponent<T>();
        }

        return comps;
    }
}
