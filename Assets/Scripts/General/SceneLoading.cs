﻿using UnityEngine.SceneManagement;

/*! \class SceneLoading
 *  \brief Controls all the unity scenes loading.
 */
public class SceneLoading
{
    private const int IndexMenu = 0;                                    //!< Build index of Menu scene.    
    private const int IndexLevel1 = 1;                                                                        

    /// <summary>
    /// Loads the menu scene.
    /// </summary>
    public static void GoToMenuScene()
    {
        SceneManager.LoadScene(IndexMenu);
    }    

    /// <summary>
    /// Loads the game scene.
    /// </summary>
    public static void StartGameScene()
    {
        SceneManager.LoadScene(IndexLevel1);
    }    

    public static void GoToNextLevelScene ()
    {
        int actualSceneIndex = SceneManager.GetActiveScene().buildIndex;
        actualSceneIndex++;
        SceneManager.LoadScene(actualSceneIndex);
    }

    public static void RestartLevelScene ()
    {
        int actualSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(actualSceneIndex);
    }
}
