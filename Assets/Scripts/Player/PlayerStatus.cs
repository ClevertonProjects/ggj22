using UnityEngine;

public class PlayerStatus : MonoBehaviour
{
    [SerializeField] private PlayersNames playerName;
    private bool isProtected;    

    public PlayersNames PlayerName { get { return playerName; } }    
    public bool IsProtected { get { return isProtected; } set { isProtected = value; } }    
}
