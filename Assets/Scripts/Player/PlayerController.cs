using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterConfigs charConfig;
    [SerializeField] private LayerMask collisionMask;

    private Vector2 inputDir;
    private Vector2 actualDir;
    private bool isBlocked;
    private Transform myTransform;

    private RaycastHit[] hits = new RaycastHit[1];        

    // Start is called before the first frame update
    void Start()
    {
        myTransform = transform;
    }

    private void Update()
    {
        ClampMovement();
        Move();
    }

    private void ClampMovement ()
    {        
        int h = Mathf.RoundToInt(inputDir.x);
        int v = Mathf.RoundToInt(inputDir.y);
        actualDir = new Vector2(h, v);
        Vector3 dir;

        if (h != 0)
        {
            dir = Vector3.right * h;           

            if (Physics.SphereCastNonAlloc(myTransform.position, charConfig.CollisionRadius, dir, hits, charConfig.CollisionCheckDist, collisionMask) > 0)
            {
                actualDir.x = 0;
            }
        }

        if (v != 0)
        {
            dir = Vector3.forward * v;

            if (Physics.SphereCastNonAlloc(myTransform.position, charConfig.CollisionRadius, dir, hits, charConfig.CollisionCheckDist, collisionMask) > 0)
            {
                actualDir.y = 0;
            }
        }
    }

    private void Move ()
    {
        Vector3 movingDir = new Vector3(actualDir.x, 0f, actualDir.y);
        myTransform.position += charConfig.MoveSpeed * Time.deltaTime * movingDir;
    }

    public void ReadInput (InputAction.CallbackContext context)
    {
        if (isBlocked)
        {
            inputDir = Vector2.zero;
        }
        else
        {
            inputDir = context.ReadValue<Vector2>();
        }
    }

    public void BlockInputs (bool isBlocked)
    {
        this.isBlocked = isBlocked;
    }

    public void Kill ()
    {
        enabled = false;
    }
}
