/*! FadeState enum, Lists the types of fading.*/
public enum FadeState
{
    FadeOut,
    FadeIn,
}