using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsTransitions : MonoBehaviour
{
    [SerializeField] private Animator anim;                                  //!< Animator component.

    private readonly int Highlight = Animator.StringToHash("Highlight");
    private readonly int Deselect = Animator.StringToHash("Deselect");
    private readonly int Click = Animator.StringToHash("Click");

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    /// <summary>
    /// Play the highlight animation.
    /// </summary>
    public void PlayHighlightAnimation()
    {
        anim.SetTrigger(Highlight);
    }

    /// <summary>
    /// Play the deselect animation.
    /// </summary>
    public void PlayDeselectAnimation()
    {
        anim.SetTrigger(Deselect);
    }

    /// <summary>
    /// Play the click animation.
    /// </summary>
    public void PlayClickAnimation()
    {
        anim.SetTrigger(Click);
    }
}
