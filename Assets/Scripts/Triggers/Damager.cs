using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    [SerializeField] private PlayersNames damageReceiver;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerStatus player = other.GetComponent<PlayerStatus>();

            if (player.PlayerName == damageReceiver && !player.IsProtected)
            {
                other.GetComponent<PlayerController>().Kill();
                GameController gc = ComponentGetter.Get<GameController>(Tags.GameController);
                gc.GameOver();
            }
        }
    }
}
