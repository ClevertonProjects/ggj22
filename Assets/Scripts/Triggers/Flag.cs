using UnityEngine;

public class Flag : MonoBehaviour
{
    [SerializeField] private Animator anim;
    private int playersInArea;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            if (playersInArea == 0)
            {
                anim.SetBool("Activate", true); 
            }

            playersInArea++;
            PlayersNames player = other.GetComponent<PlayerStatus>().PlayerName;
            ComponentGetter.Get<GameController>(Tags.GameController).OnLevelTargetEnter(player);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            playersInArea--;

            if (playersInArea <= 0)
            {
                playersInArea = 0;
                anim.SetBool("Activate", false);
            }

            PlayersNames player = other.GetComponent<PlayerStatus>().PlayerName;
            ComponentGetter.Get<GameController>(Tags.GameController).OnLevelTargetExit(player);
        }
    }
}
