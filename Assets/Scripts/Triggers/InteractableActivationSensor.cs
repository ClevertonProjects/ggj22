using UnityEngine;

public class InteractableActivationSensor : MonoBehaviour
{
    [SerializeField] private GameObject instructionCanvas;
    [SerializeField] private ControllableObjects controllableObject;    

    private void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag(Tags.Player) && controllableObject.PlayerInQueue == null)
        {
            instructionCanvas.SetActive(true);
            controllableObject.PlayerInQueue = other.transform;            
        }
    }

    private void OnTriggerExit (Collider other)
    {
        if (other.CompareTag(Tags.Player) && controllableObject.PlayerInQueue == other.transform)
        {
            instructionCanvas.SetActive(false);
            controllableObject.PlayerInQueue = null;            
        }
    }    
}
