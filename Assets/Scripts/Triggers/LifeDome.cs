using UnityEngine;

public class LifeDome : MonoBehaviour
{
    [SerializeField] private PlayersNames damageReceiver;

    [SerializeField] private bool hasPlayer;

    public PlayersNames DamageReceiver { get { return damageReceiver; } }    
    public bool HasPlayer { get { return hasPlayer; } }

    private void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerStatus player = other.GetComponent<PlayerStatus>();

            if (player.PlayerName == damageReceiver)
            {
                hasPlayer = true;
            }
        }
    }

    private void OnTriggerExit (Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerStatus player = other.GetComponent<PlayerStatus>();

            if (player.PlayerName == damageReceiver)
            {
                hasPlayer = false;
            }
        }
    }
}
