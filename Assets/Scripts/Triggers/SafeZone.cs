using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeZone : MonoBehaviour
{
    [SerializeField] private PlayersNames protectedPlayer;

    [SerializeField] private SafeZone linkedSafeZone;
    private bool hasPlayer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerStatus player = other.GetComponent<PlayerStatus>();

            if (player.PlayerName == protectedPlayer)
            {
                player.IsProtected = true;
                hasPlayer = true;                
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            PlayerStatus player = other.GetComponent<PlayerStatus>();

            if (player.PlayerName == protectedPlayer)
            {
                hasPlayer = false;

                if (linkedSafeZone != null && linkedSafeZone.hasPlayer)
                {
                    
                }   
                else
                {
                    player.IsProtected = false;
                }
            }
        }
    }
}
