using UnityEngine;

[CreateAssetMenu(fileName = "New Char Config", menuName = "Custom/Player Config", order = 0)]
public class CharacterConfigs : ScriptableObject
{
    public float MoveSpeed;

    public float CollisionRadius;
    public float CollisionCheckDist;
}
