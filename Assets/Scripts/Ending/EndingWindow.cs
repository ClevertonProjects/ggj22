using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingWindow : MonoBehaviour
{   
    [SerializeField] private UiGroupFader creditsPanel;                                 //!< Credits panel animation.
    [SerializeField] private float screenDuration;
        
    public void ShowWindow()
    {
        creditsPanel.FadeIn();
        DelayedCall.Invoke(ReturnToMenu, screenDuration, this);
    }
    
    private void ReturnToMenu ()
    {
        SceneFader fader = ComponentGetter.Get<SceneFader>(Tags.Fader);
        fader.FadeScene(FadeState.FadeIn, SceneLoading.GoToMenuScene);
    }
}
