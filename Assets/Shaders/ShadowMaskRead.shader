Shader "Custom/ShadowMaskRead"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        [IntRange] _StencilRef ("Stencil Ref Value", Range(0,255)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Geometry" }

        Stencil
        {
            Ref [_StencilRef]
            Comp NotEqual
        }        

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;                
            };

            struct v2f
            {                
                float4 vertex : SV_POSITION;
            };

            fixed4 _Color;            

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _Color;
            }
            ENDCG
        }
    }
}
